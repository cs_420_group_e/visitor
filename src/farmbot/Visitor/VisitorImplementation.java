package farmbot.Visitor;

import farmbot.Component.Item;
import farmbot.Component.ItemContainer;

public class VisitorImplementation implements ComponentVisitor {

	private double[] arr = new double[2];
	
	@Override
	public double[] visit(Item item) {
		arr[0] += item.getPrice();
		arr[1] += item.getMarketValue();
		return arr;
	}

	@Override
	public double[] visit(ItemContainer container) {
		for(int i = 0; i < container.getContainerSize(); i++) {
			if(container.getList().get(i) instanceof ItemContainer) {
				visit((ItemContainer) container.getList().get(i));
				
			} else {	
				visit((Item) container.getList().get(i));
			}
			
		}
		arr[0] += container.getPrice();
		arr[1] += container.getMarketValue();
		return arr;
	}

}
