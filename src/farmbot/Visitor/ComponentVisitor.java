package farmbot.Visitor;
import farmbot.Component.Item;
import farmbot.Component.ItemContainer;

public interface ComponentVisitor {
	double[] visit(Item item); 
	double[] visit(ItemContainer container);
}
