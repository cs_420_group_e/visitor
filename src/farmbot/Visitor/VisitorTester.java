package farmbot.Visitor;

import farmbot.Component.Component;
import farmbot.Component.Item;
import farmbot.Component.ItemContainer;

public class VisitorTester {

	public static void main(String[] args) {
		ItemContainer barn = new ItemContainer("Barn", 1000, 5, 5, 10, 20);
		Item cow = new Item("Cow", 100, 20, 20, 1, 1);
		Item chicken = new Item("Chicken", 50, 30, 30, 1, 1);
		ItemContainer box = new ItemContainer("Equipment Storage", 500, 40, 40, 10, 5);
		Item fertilizer = new Item("Fertilizer", 50, 50, 7, 1, 1);
		Item hoe = new Item("Hoe", 60, 60, 6, 1, 1);
		
		barn.addItem(cow);
		barn.addItem(chicken);
		barn.addItem(box);
		box.addItem(fertilizer);
		box.addItem(hoe);
        
        System.out.println(barn.getName() + ": currentPrice = " + getCurrentPrice(barn) + 
        		" marketValue = " + getMarketValue(barn));
        
        System.out.println(box.getName() + ": currentPrice = " + getCurrentPrice(box) + 
        		" marketValue = " + getMarketValue(box));
        
	}
	
	public static double getCurrentPrice(Component c) {
		// index 0 for currentPrice
		return c.accept(new VisitorImplementation())[0];
	}
	
	public static double getMarketValue(Component c) {
		// index 1 for marketValue
		return c.accept(new VisitorImplementation())[1];
	}

}
