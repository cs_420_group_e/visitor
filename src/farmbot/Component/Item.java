package farmbot.Component;

import java.util.Iterator;

import farmbot.Visitor.ComponentVisitor;
import farmbot.Visitor.Visitor;

/**
 *
 * @author John Stephenson, Uzma Nur
 */
public class Item extends Component implements Visitor {

    private String name;
    private double price;
    private double xLoc;
    private double yLoc;
    private double length;
    private double width;
    private double currentMarketValue;

    public Item(String name, double price, double xLoc, double yLoc, double length, double width){
        this.name = name;
        this.price = price;
        this.xLoc = xLoc;
        this.yLoc = yLoc;
        this.length = length;
        this.width = width;
        this.currentMarketValue = price + price * .05;
    }

    @Override
    public void changeName(String name) {
        this.name = name;
    }

    @Override
    public void changePrice(double price) {
        this.price = price;
    }

    @Override
    public void changeLocationX(double xLoc) {
        this.xLoc = xLoc;
    }

    @Override
    public void changeLocationY(double yLoc) {
        this.yLoc = yLoc;
    }

    @Override
    public void changeLength(double length) {
        this.length = length;
    }

    @Override
    public void changeWidth(double width) {
        this.width = width;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getPrice() { return this.price; }

    @Override
    public double getLocationX() {
        return this.xLoc;
    }

    @Override
    public double getLocationY() {
        return this.yLoc;
    }

    @Override
    public double getLength() {
        return this.length;
    }

    @Override
    public double getWidth() {
        return this.width;
    }
    
    @Override
    public double getMarketValue() {
    	return this.currentMarketValue;
    }

	@Override
	public double[] accept(ComponentVisitor visitor) {
		return visitor.visit(this);
	}

}

