package farmbot.Component;

import farmbot.Visitor.ComponentVisitor;

/**
 *
 * @author John Stephenson, Uzma Nur
 */
public abstract class Component {
    public abstract void changeName(String name);
    public abstract void changePrice(double price);
    public abstract void changeLocationX(double xLoc);
    public abstract void changeLocationY(double yLoc);
    public abstract void changeLength(double length);
    public abstract void changeWidth(double width);
    public abstract String getName();
    public abstract double getPrice();
    public abstract double getLocationX();
    public abstract double getLocationY();
    public abstract double getLength();
    public abstract double getWidth();
    public abstract double getMarketValue();
    public abstract double[] accept(ComponentVisitor visitor);
}
