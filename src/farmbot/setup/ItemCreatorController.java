package farmbot.setup;

import farmbot.Component.Component;
import farmbot.Component.Item;
import farmbot.Component.ItemContainer;
import farmbot.FarmBot;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Nick
 */
public class ItemCreatorController implements Initializable {

    @FXML
    private TextField widthText;
    @FXML
    private TextField lengthText;
    @FXML
    private TextField yLocText;
    @FXML
    private TextField xLocText;
    @FXML
    private TextField priceText;
    @FXML
    private TextField nameText;
    @FXML
    private Button addItemButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // decimals only
        priceText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    priceText.setText(oldValue);
                }
            }
        });
        
        // decimals only
        xLocText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    xLocText.setText(oldValue);
                }
            }
        });
        
        // decimals only
        yLocText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    yLocText.setText(oldValue);
                }
            }
        });
        
        // integers only
        lengthText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    lengthText.setText(oldValue);
                }
            }
        });
        
        // integers only
        widthText.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                    widthText.setText(oldValue);
                }
            }
        });
    }    

    @FXML
    private void addItem(ActionEvent event) {
        // I probably shouldve used an array oops
        if (nameText.getText().trim().isEmpty() || priceText.getText().trim().isEmpty() || xLocText.getText().trim().isEmpty() || yLocText.getText().trim().isEmpty() || lengthText.getText().trim().isEmpty() || widthText.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please input data in all fields.");
            alert.showAndWait();
        } else {
            // set the itemcontainer from the user input
            Item item = new Item(nameText.getText().trim(), Double.parseDouble(priceText.getText().trim()), Double.parseDouble(xLocText.getText()), Double.parseDouble(yLocText.getText()), Double.parseDouble(lengthText.getText()), Double.parseDouble(widthText.getText()));
            
            // add the item to the currently selected component (if no selected, it is default root)
            ((ItemContainer)FarmBot.getInstance().getCurrentComponent()).addItem(item);
            // add the item container to the tree
            final TreeItem<Component> child1 = new TreeItem<>(item);
            FarmBot.getInstance().getCurrentTreeItem().getChildren().add(child1);
            
            Stage stage = (Stage) addItemButton.getScene().getWindow();
            stage.close();
        }
    }
    
}
